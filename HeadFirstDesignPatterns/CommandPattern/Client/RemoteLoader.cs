﻿using System;
using CommandPattern.Command;
using CommandPattern.Invoker;
using CommandPattern.Receiver;

namespace CommandPattern.Client
{
    public class RemoteLoader
    {
        public RemoteLoader()
        {
            var remoteControl = new RemoteControl();

            var livingRoomLight = new Light("Living Room");
            var kitchenLight = new Light("Kitchen");
            var garageDoor = new GarageDoor("");
            var stereo = new Stereo("Living Room");
            var ceilingFan = new CeilingFan("Living Room");

            var livingRoomLightOn = new LightOnCommand(livingRoomLight);
            var livingRoomLightOff = new LightOffCommand(livingRoomLight);

            var kitchenLightOn = new LightOnCommand(kitchenLight);
            var kitchenLightOff = new LightOffCommand(kitchenLight);

            var garageDoorUp = new GarageDoorUpCommand(garageDoor);
            var garageDoorDown = new GarageDoorDownCommand(garageDoor);

            var ceilingFanHigh = new CeilingFanHighCommand(ceilingFan);
            var ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan);
            var ceilingFanLow = new CeilingFanLowCommand(ceilingFan);
            var ceilingFanOff = new CeilingFanOffCommand(ceilingFan);

            var stereoOnWithCd = new StereoOnWithCDCommand(stereo);

            var stereoOff = new StereoOffCommand(stereo);

            remoteControl.SetCommand(0, livingRoomLightOn, livingRoomLightOff);
            remoteControl.SetCommand(1, kitchenLightOn, kitchenLightOff);
            remoteControl.SetCommand(2, stereoOnWithCd, stereoOff);

            remoteControl.SetCommand(3, ceilingFanHigh, ceilingFanOff);
            remoteControl.SetCommand(4, ceilingFanMedium, ceilingFanOff);
            remoteControl.SetCommand(5, ceilingFanLow, ceilingFanOff);

            Console.WriteLine(remoteControl.ToString());

            remoteControl.OnButtonWasPushed(0);
            remoteControl.OffButtonWasPushed(0);
            remoteControl.UndoButtonWasPushed();
            remoteControl.OffButtonWasPushed(0);
            remoteControl.OnButtonWasPushed(0);

            remoteControl.OnButtonWasPushed(1);
            remoteControl.OffButtonWasPushed(1);


            remoteControl.OnButtonWasPushed(2);
            remoteControl.OffButtonWasPushed(2);


            remoteControl.OnButtonWasPushed(4); // Set to medium
            remoteControl.OffButtonWasPushed(4); // Turn off

            remoteControl.UndoButtonWasPushed(); // Should go back to medium
            remoteControl.OnButtonWasPushed(3); // Set to High
            remoteControl.UndoButtonWasPushed(); // should go back to medium

            ICommand[] partyOn = {livingRoomLightOn, stereoOnWithCd, garageDoorUp, ceilingFanHigh};
            ICommand[] partyOff = {livingRoomLightOff, stereoOff, garageDoorDown, ceilingFanOff};

            var partyOnMacro = new MacroCommand(partyOn);
            var partyOffMacro = new MacroCommand(partyOff);

            remoteControl.SetCommand(0, partyOnMacro, partyOffMacro);

            Console.WriteLine("--- Pushing Macro On ---");
            remoteControl.OnButtonWasPushed(0);
            Console.WriteLine("--- Pushing Macro Off ---");
            remoteControl.OffButtonWasPushed(0);
        }
    }
}