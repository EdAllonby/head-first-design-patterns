﻿using CommandPattern.Receiver;

namespace CommandPattern.Command
{
    public class CeilingFanHighCommand : ICommand
    {
        private readonly CeilingFan ceilingFan;
        private int previousSpeed;

        public CeilingFanHighCommand(CeilingFan ceilingFan)
        {
            this.ceilingFan = ceilingFan;
        }

        public void Execute()
        {
            previousSpeed = ceilingFan.GetSpeed();
            ceilingFan.High();
        }

        public void Undo()
        {
            switch (previousSpeed)
            {
                case CeilingFan.HIGH:
                    ceilingFan.High();
                    break;
                case CeilingFan.MEDIUM:
                    ceilingFan.Medium();
                    break;
                case CeilingFan.LOW:
                    ceilingFan.Low();
                    break;
                case CeilingFan.OFF:
                    ceilingFan.Off();
                    break;
            }
        }
    }
}