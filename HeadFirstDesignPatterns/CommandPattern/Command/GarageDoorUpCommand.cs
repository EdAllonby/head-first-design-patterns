﻿using CommandPattern.Receiver;

namespace CommandPattern.Command
{
    internal class GarageDoorUpCommand : ICommand
    {
        private readonly GarageDoor garageDoor;

        public GarageDoorUpCommand(GarageDoor garageDoor)
        {
            this.garageDoor = garageDoor;
        }

        public void Execute()
        {
            garageDoor.Up();
        }

        public void Undo()
        {
            garageDoor.Down();
        }
    }
}