﻿using System;

namespace CommandPattern.Command
{
    internal class NoCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("No command set for this slot");
        }

        public void Undo()
        {
            Console.WriteLine("No command set for this slot");
        }
    }
}