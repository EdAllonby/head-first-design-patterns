﻿using CommandPattern.Receiver;

namespace CommandPattern.Command
{
    internal class GarageDoorDownCommand : ICommand
    {
        private readonly GarageDoor garageDoor;

        public GarageDoorDownCommand(GarageDoor garageDoor)
        {
            this.garageDoor = garageDoor;
        }

        public void Execute()
        {
            garageDoor.Down();
        }

        public void Undo()
        {
            garageDoor.Up();
        }
    }
}