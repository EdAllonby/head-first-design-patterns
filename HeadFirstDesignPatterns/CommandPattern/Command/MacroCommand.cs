﻿namespace CommandPattern.Command
{
    internal class MacroCommand : ICommand
    {
        private readonly ICommand[] commands;

        public MacroCommand(ICommand[] commands)
        {
            this.commands = commands;
        }

        public void Execute()
        {
            foreach (ICommand command in commands)
            {
                command.Execute();
            }
        }

        public void Undo()
        {
            foreach (ICommand command in commands)
            {
                command.Undo();
            }
        }
    }
}