﻿using System;

namespace CommandPattern.Receiver
{
    public class CeilingFan
    {
        public const int HIGH = 3;
        public const int MEDIUM = 2;
        public const int LOW = 1;
        public const int OFF = 0;
        private string location;
        private int speed;

        public CeilingFan(string location)
        {
            this.location = location;
        }

        public void High()
        {
            Console.WriteLine("Ceiling Fan on high");
            speed = HIGH;
        }

        public void Medium()

        {
            Console.WriteLine("Ceiling Fan on medium");
            speed = MEDIUM;
        }

        public void Low()
        {
            Console.WriteLine("Ceiling Fan on low");
            speed = LOW;
        }

        public void Off()
        {
            Console.WriteLine("Ceiling Fan off");
            speed = OFF;
        }

        public int GetSpeed()
        {
            return speed;
        }
    }
}