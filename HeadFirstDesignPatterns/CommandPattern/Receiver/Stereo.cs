﻿using System;

namespace CommandPattern.Receiver
{
    internal class Stereo
    {
        private readonly string name;

        public Stereo(string name)
        {
            this.name = name;
        }

        public void On()
        {
            Console.WriteLine("{0} stereo is On.", name);
        }

        public void Off()
        {
            Console.WriteLine("{0} stereo is Off.", name);
        }

        public void SetCd()
        {
            Console.WriteLine("{0} stereo Cd is set", name);
        }

        public void SetDvd()
        {
            Console.WriteLine("{0} stereo DVD is set", name);
        }

        public void SetRadio()
        {
            Console.WriteLine("{0} stereo Radio is set", name);
        }

        public void SetVolume(int volume)
        {
            Console.WriteLine("{0} stereo Volume is set to: {1}", name, volume);
        }
    }
}