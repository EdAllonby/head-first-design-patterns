﻿using System;

namespace CommandPattern.Receiver
{
    public class Light
    {
        private readonly string name;

        public Light(string name)
        {
            this.name = name;
        }

        public void On()
        {
            Console.WriteLine("{0} light turned on.", name);
        }

        public void Off()
        {
            Console.WriteLine("{0} light turned off.", name);
        }
    }
}