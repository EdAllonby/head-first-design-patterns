﻿using System;

namespace CommandPattern.Receiver
{
    internal class GarageDoor
    {
        private readonly string name;

        public GarageDoor(string name)
        {
            this.name = name;
        }

        public void Up()
        {
            Console.WriteLine("{0} garage door is up.", name);
        }

        public void Down()
        {
            Console.WriteLine("{0} garage door is down.", name);
        }

        public void Stop()
        {
            Console.WriteLine("{0} garage door has stopped.", name);
        }

        public void LightOn()
        {
            Console.WriteLine("{0} garage door light is on.", name);
        }

        public void LightOff()
        {
            Console.WriteLine("{0} garage door light is off.", name);
        }
    }
}