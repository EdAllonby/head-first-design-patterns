﻿using System;
using CommandPattern.Client;

namespace HeadFirstDesignPatterns
{
    internal static class Program
    {
        private static void Main()
        {
            RunCommandPattern();
        }

        private static void RunCommandPattern()
        {
            Console.WriteLine("Command Pattern Started.");
            var remoteControlTest = new RemoteLoader();
        }
    }
}